library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity Semaforo_tb is
end;

architecture bench of Semaforo_tb is

component Semaforo is
    Port(
        Pulsador, sensor, reset, clk: IN std_logic;
        display_number: OUT std_logic_vector(6 downto 0);
        selector_display: OUT std_logic_vector(7 downto 0);
        RP,  AP,  VP: OUT std_logic;
        RS,  AS,  VS: OUT std_logic;
        RPP,  VPP: OUT std_logic;
        RSP,  VSP: OUT std_logic
        );
end component;

  signal Pulsador, sensor, reset, clk: std_logic;
  signal display_number: std_logic_vector(6 downto 0);
  signal selector_display: std_logic_vector(7 downto 0);
  signal RP,AP,VP: std_logic;
  signal RS,AS,VS: std_logic;
  signal RPP,VPP: std_logic;
  signal RSP,VSP: std_logic ;
  
  constant K: time := 10 ns;
  signal stop_the_clock: boolean;

begin

  uut: Semaforo port map ( clk    => clk,
                           reset  => reset,
                           Pulsador      => Pulsador,
                           Sensor      => Sensor,
                           RP               => RP,
                           AP               => AP,
                           VP               => VP,
                           RS               => RS,
                           AS               => AS,
                           VS               => VS,
                           RPP              => RPP,
                           VPP              => VPP,
                           RSP              => RSP,
                           VSP              => VSP,
                           display_number => display_number,
                           selector_display => selector_display );

  stimulus: process
  begin
  
    -- Put initialisation code here
    reset <= '0';
    Pulsador <= '0';
    Sensor <= '0';
    wait for 20 ns;
    Pulsador <= '1';
    Pulsador <= '1';
    wait for 60 ns;
    Pulsador <= '0';
    wait for 500 ns;
    
    sensor <= '1';
    wait for 60 ns;
    sensor <= '0';
    wait for 500 ns;
    
    Pulsador <= '1';
    wait for 60 ns;
    Pulsador <= '0';
    wait for 200 ns;
    reset <= '1';
    wait for 20 ns;
    reset <= '0';
    
    
    -- Put test bench stimulus code here
    stop_the_clock <= true;
  end process;

clocking: process
  begin
    while not stop_the_clock loop
      clk <= '0', '1' after K / 2;
      wait for K;
    end loop;
    wait;
  end process;

end;