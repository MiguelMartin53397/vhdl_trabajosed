library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Semaforo is
    Port(
        Pulsador, sensor, reset, clk: IN std_logic;
        display_number: OUT std_logic_vector(6 downto 0);
        selector_display: OUT std_logic_vector(7 downto 0);
        RP,  AP,  VP: OUT std_logic;
        RS,  AS,  VS: OUT std_logic;
        RPP,  VPP: OUT std_logic;
        RSP,  VSP: OUT std_logic
        );
end Semaforo;

architecture Structural of Semaforo is

component Estados is
    port(
            clk, reset, pulsador, sensor: in std_logic;
            fin: in std_logic_vector(3 downto 0);
            estados: out std_logic_vector (3 downto 0)
        );
end component;

component Contador is
    generic ( FACTOR: NATURAL );
    port(
            clk, reset: in std_logic;
            ce_n: in std_logic;
            fin: OUT std_logic;
            count: out std_logic_vector (4 downto 0)
        );
end component;

component gestorLuces is
  Port (
    signal clk: IN std_logic;
    signal estado: IN std_logic_vector(3 downto 0);
    signal RP,  AP,  VP: OUT std_logic;
    signal RS,  AS,  VS: OUT std_logic;
    signal RPP,  VPP: OUT std_logic;
    signal RSP,  VSP: OUT std_logic
        );
end component;

component Div_frec is
generic ( FACTOR : POSITIVE );
  Port (
    signal clk: IN std_logic;
    signal clk_s: OUT std_logic;
    signal reset: IN std_logic
        );
end component;

component Selector is
    Port(
        clk: in std_logic;
        estado: in std_logic_vector(3 downto 0);
        deco_9: in std_logic_vector(6 downto 0);
        deco_sC_dec_seg: in std_logic_vector(6 downto 0);
        deco_sC_seg: in std_logic_vector(6 downto 0);
        deco_3_estado_2: in std_logic_vector(6 downto 0);
        deco_3_estado_4: in std_logic_vector(6 downto 0);
        number_dec_seg: out std_logic_vector(6 downto 0);
        number_seg: out std_logic_vector(6 downto 0)
        );
end component;

component separadorCifras is
  Port ( 
    signal clk: IN std_logic;
    signal cuenta: IN std_logic_vector(4 downto 0);
    signal display_number_dec_seg: OUT std_logic_vector(4 downto 0);
    signal display_number_seg: OUT std_logic_vector(4 downto 0)
        );
end component;

component Display_refresh is
  Port ( 
  signal clk: IN std_logic;
  signal display_number_se: IN std_logic_vector (6 downto 0);
  signal display_number_dec_se: IN std_logic_vector (6 downto 0);
  signal display_selection: OUT std_logic_vector (7 downto 0);
  signal display_number: OUT std_logic_vector (6 downto 0)
  );
end component;

component Decoder IS
PORT(
    code : IN std_logic_vector(4 DOWNTO 0);
    led : OUT std_logic_vector(6 DOWNTO 0)
     );
END component;

component INPUT_CNDTNR is
  port (
    RESET_N  : in  std_logic;
    CLK      : in  std_logic;
    ASYNC_IN : in  std_logic;
    EDGE     : out std_logic
  );
end component;

--signal via_pulsador, via_sensor: std_logic;
signal via_estados: std_logic_vector(3 downto 0);
signal contador_to_estados: std_logic_vector(3 downto 0);
signal count_9s: std_logic_vector(4 downto 0);
signal count_3s: std_logic_vector(4 downto 0);
signal count_30s: std_logic_vector(4 downto 0);
signal count_3sb: std_logic_vector(4 downto 0);
signal clk_m, clk_3: std_logic;
Signal separador_To_Deco_Dec_seg: std_logic_vector(4 downto 0);
Signal separador_To_Deco_seg: std_logic_vector(4 downto 0);
Signal deco_30s_Dec_seg_to_selector: std_logic_vector(6 downto 0);
Signal deco_30s_seg_to_selector: std_logic_vector(6 downto 0);
Signal contador_9s_to_selector: std_logic_vector(6 downto 0);
Signal contador_3s_to_selector: std_logic_vector(6 downto 0);
Signal contador_3sb_to_selector: std_logic_vector(6 downto 0);
Signal dec_Seg_from_selector_to_DR: std_logic_vector(6 downto 0);
Signal Seg_from_selector_to_DR: std_logic_vector(6 downto 0);

begin

--ins_Deb_P: INPUT_CNDTNR
--        port map(
--            clk => clk,
--            reset_n => reset,
--            async_in => pulsador,
--            edge => via_pulsador
--                );

--ins_Deb_S: INPUT_CNDTNR
--        port map(
--            clk => clk,
--            reset_n => reset,
--            async_in => sensor,
--            edge => via_sensor
--                );

inst_maquiEstados: Estados
        port map(
            clk => clk,
            reset => reset,
            pulsador => pulsador,
            sensor => sensor,
            estados => via_estados,
            fin(0) => contador_to_estados(0),
            fin(1) => contador_to_estados(1),
            fin(2) => contador_to_estados(2),
            fin(3) => contador_to_estados(3)
                );

inst_Contador_9s: Contador
        generic map( FACTOR => 9 )
        port map(
            reset => reset,
            clk => clk_3,
            ce_n => via_estados(0),
            fin => contador_to_estados(0),
            count => count_9s
                );
                
inst_Contador_3s: Contador
        generic map( FACTOR => 3 )
        port map(
            reset => reset,
            clk => clk_3,
            ce_n => via_estados(1),
            fin => contador_to_estados(1),
            count => count_3s
                );                                
                
inst_Contador_30s: Contador
        generic map( FACTOR => 30 )
        port map(
            reset => reset,
            clk => clk_3,
            ce_n => via_estados(2),
            fin => contador_to_estados(2),
            count => count_30s
                );
                
inst_Contador_3sb: Contador
        generic map( FACTOR => 3 )
        port map(
            reset => reset,
            clk => clk_3,
            ce_n => via_estados(3),
            fin => contador_to_estados(3),
            count => count_3sb
        );                
                
inst_gesLuces: gestorLuces 
        port map(
            clk => clk,
            estado => via_estados,
            RP => RP,  AP => AP,  VP => VP,
            RS => RS,  AS => AS,  VS => VS,
            RPP => RPP,  VPP => VPP,  
            RSP => RSP,  VSP => VSP           
        );
        
inst_divf: Div_frec
        generic map( FACTOR => 50000000 )
        port map(
            clk => clk,
            clk_s => clk_3,
            reset => reset
        );
        
inst_divf2: Div_frec
        generic map( FACTOR => 100000 )
        port map(
            clk => clk,
            clk_s => clk_m,
            reset => reset
        );
        
inst_sepCifras: separadorCifras 
        port map ( 
            clk => clk,
            cuenta => count_30s,
            display_number_dec_seg => separador_To_Deco_Dec_seg,
            display_number_seg => separador_To_Deco_seg
        );

inst_Deco1: Decoder 
        port map (
            code => separador_To_Deco_Dec_seg,
            led => deco_30s_Dec_seg_to_selector
             );
inst_Deco2: Decoder 
        port map (
            code => separador_To_Deco_seg,
            led => deco_30s_seg_to_selector
             );

inst_Deco3: Decoder 
        port map (
            code => count_9s,
            led => contador_9s_to_selector
             );

inst_Deco4: Decoder 
        port map (
            code => count_3s,
            led => contador_3s_to_selector
             );

inst_Deco5: Decoder 
        port map (
            code => count_3sb,
            led => contador_3sb_to_selector
             );

inst_select: Selector 
        port map(
            clk => clk,
            estado => via_estados,
            deco_9 => contador_9s_to_selector,
            deco_sC_dec_seg => deco_30s_Dec_seg_to_selector,
            deco_sC_seg =>  deco_30s_seg_to_selector,
            deco_3_estado_2 => contador_3s_to_selector,
            deco_3_estado_4 => contador_3sb_to_selector,
            number_dec_seg => Dec_Seg_from_selector_to_DR,
            number_seg => Seg_from_selector_to_DR
        );
    
        
inst_dispRefresh: Display_refresh
        port map ( 
            clk => clk_m,
            display_number_se => Seg_from_selector_to_DR,
            display_number_dec_se => Dec_Seg_from_selector_to_DR,
            display_selection => selector_display,
            display_number => display_number
  );
     
    
end structural;