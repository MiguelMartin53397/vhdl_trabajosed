library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;


entity Estados is
    port(
            clk, reset, pulsador, sensor: in std_logic;
            fin: in std_logic_vector(3 downto 0);
            estados: out std_logic_vector (3 downto 0) := "0000"
        );
end Estados;

architecture Behavioral of Estados is
TYPE state_type IS (s0,s1,s2,s3,s4);
signal estado: state_type := s0;
begin
    --estado <= s0;
    process ( clk, reset )
    begin
        if reset = '1' then 
            estado <= s0;
        elsif rising_edge(clk) then 
            if pulsador = '1' or sensor = '1' then
                estado <= s1;
            elsif fin(0) = '1' AND estado = s1 then
                estado <= s2;
            elsif fin(1) = '1' AND estado = s2 then
                estado <= s3;
            elsif fin(2) = '1' AND estado = s3 then
                estado <= s4;
            elsif fin(3) = '1' AND estado = s4 then
                estado <= s0;
         end if;
         case estado is
            when s0 => estados <= "0000";
            when s1 => estados <= "0001";
            when s2 => estados <= "0010";
            when s3 => estados <= "0100";
            when s4 => estados <= "1000";
            when others => estados <= "0000";
        end case;
         end if;
     end process; 
end architecture;