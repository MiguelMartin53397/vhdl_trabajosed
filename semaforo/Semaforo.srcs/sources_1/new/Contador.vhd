library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.Numeric_Std.all;

entity Contador is
    generic ( FACTOR: NATURAL );
    port(
            clk, reset: in std_logic;
            ce_n: in std_logic;
            fin: OUT std_logic;
            count: out std_logic_vector (4 downto 0)
        );
end Contador;

architecture Behavioral of Contador is
begin
    process ( clk )
        VARIABLE cuenta: INTEGER range 0 to FACTOR := FACTOR;
    begin
        fin <= '0';
        if reset = '1' then
            cuenta := FACTOR;
        elsif rising_edge( clk ) then
            if cuenta = 0 then
                cuenta := FACTOR;
                fin <= '1';
            elsif ce_n = '1' then
                cuenta := cuenta - 1;
            end if;
        end if;
        count <= std_logic_vector(to_unsigned(cuenta,count'length));
    end process;
end Behavioral;