library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity separadorCifras is
  Port ( 
    signal clk: IN std_logic;
    signal cuenta: IN std_logic_vector(4 downto 0);
    signal display_number_dec_seg: OUT std_logic_vector(4 downto 0);
    signal display_number_seg: OUT std_logic_vector(4 downto 0)
        );
end separadorCifras;

architecture Behavioral of separadorCifras is

begin
    process( clk )
        VARIABLE dec_seg, seg: INTEGER range 0 to 31;
    begin
        dec_seg := to_integer(unsigned(cuenta));
        if dec_seg >= 10 then
            seg := dec_seg REM 10;
            dec_seg := dec_seg - seg;
            dec_seg := dec_seg / 10;
        elsif dec_seg <10 then
            seg := dec_seg;
            dec_seg := 0;
        end if;
        
        display_number_dec_seg <= std_logic_vector(to_unsigned(dec_seg,display_number_dec_seg'length));
        display_number_seg <= std_logic_vector(to_unsigned(seg,display_number_seg'length));
        
    end process;
end Behavioral;