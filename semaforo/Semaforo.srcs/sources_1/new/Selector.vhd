library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Selector is
    Port(
        clk: in std_logic;
        estado: std_logic_vector(3 downto 0);
        deco_9: in std_logic_vector(6 downto 0);
        deco_sC_dec_seg: in std_logic_vector(6 downto 0);
        deco_sC_seg: in std_logic_vector(6 downto 0);
        deco_3_estado_2: in std_logic_vector(6 downto 0);
        deco_3_estado_4: in std_logic_vector(6 downto 0);
        number_dec_seg: out std_logic_vector(6 downto 0);
        number_seg: out std_logic_vector(6 downto 0)
        );
end Selector;

architecture Behavioral of Selector is

begin
    process( clk )
    begin
        case estado is
            when "0001" =>
                number_dec_seg <= "0000001";
                number_seg <= deco_9;
            when "0010" =>
                number_dec_seg <= "0000001";
                number_seg <= deco_3_estado_2;
            when "0100" =>
                number_dec_seg <= deco_sC_dec_seg;
                number_seg <= deco_sC_seg;
            when "1000" =>
                number_dec_seg <= "0000001";
                number_seg <= deco_3_estado_4;
            when OTHERS =>
                number_dec_seg <= "0000001";
                number_seg <= "0000001";
        end case;
    end process;
end Behavioral;
