library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Display_refresh is
  Port ( 
  signal clk: IN std_logic;
  signal display_number_se: IN std_logic_vector (6 downto 0);
  signal display_number_dec_se: IN std_logic_vector (6 downto 0);
  signal display_selection: OUT std_logic_vector (7 downto 0);
  signal display_number: OUT std_logic_vector (6 downto 0)
  );
end Display_refresh;

architecture Behavioral of Display_refresh is

begin
    process( clk )
        variable cnt: integer range 0 to 1 := 0;
    begin
        if rising_edge( clk ) then
            if cnt = 2 then
                cnt := 0;
            else
                cnt := cnt + 1;
            end if;
        end if;
        case cnt is
            when 0 => 
                display_selection <= "11111110";
                display_number <= display_number_se;
            when 1 =>
                display_selection <= "11111101";
                display_number <= display_number_dec_se;
       end case;
    end process;       
end Behavioral;
