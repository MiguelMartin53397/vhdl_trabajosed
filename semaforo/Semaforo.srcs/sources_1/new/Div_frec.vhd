library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Div_frec is
generic ( FACTOR : POSITIVE );
  Port (
    signal clk: IN std_logic;
    signal clk_s: OUT std_logic;
    signal reset: IN std_logic
        );
end Div_frec;

architecture Behavioral of Div_frec is
    signal clk_state: std_logic := '0';
begin
    gen_clok: process( clk, reset )
        VARIABLE count: INTEGER range 0 to FACTOR := 0;
    begin
        if reset = '1' then 
            count := 0;
        elsif rising_edge( clk ) then
            count := count + 1;
            if count = FACTOR then
                count := 0;
                clk_state <= not clk_state;
            end if;
        end if;
    end process;
    
    salida: process( clk_state )
    begin
        clk_s <= clk_state;
    end process;
end Behavioral;
