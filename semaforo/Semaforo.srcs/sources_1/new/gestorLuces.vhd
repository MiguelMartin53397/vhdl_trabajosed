library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity gestorLuces is
  Port (
    signal clk: IN std_logic;
    signal estado: IN std_logic_vector(3 downto 0);
    signal RP,  AP,  VP: OUT std_logic;
    signal RS,  AS,  VS: OUT std_logic;
    signal RPP,  VPP: OUT std_logic;
    signal RSP,  VSP: OUT std_logic
        );
end gestorLuces;

architecture Behavioral of gestorLuces is

begin
    process( clk )
    begin
        case estado is
            when "0000" =>
                RP <= '0';   RS <= '1';   RPP <= '1';   RSP <= '0';
                AP <= '0';   AS <= '0';   VPP <= '0';   VSP <= '1';
                VP <= '1';   VS <= '0';
            when "0001" =>
                RP <= '0';   RS <= '1';   RPP <= '1';   RSP <= '0';
                AP <= '0';   AS <= '0';   VPP <= '0';   VSP <= '1';
                VP <= '1';   VS <= '0';
            when "0010" =>
                RP <= '0';   RS <= '1';   RPP <= '1';   RSP <= '0';
                AP <= '1';   AS <= '0';   VPP <= '0';   VSP <= '1';
                VP <= '0';   VS <= '0';
            when "0100" =>
                RP <= '1';   RS <= '0';   RPP <= '0';   RSP <= '1';
                AP <= '0';   AS <= '0';   VPP <= '1';   VSP <= '0';
                VP <= '0';   VS <= '1';
            when "1000" =>
                RP <= '1';   RS <= '0';   RPP <= '0';   RSP <= '1';
                AP <= '0';   AS <= '1';   VPP <= '1';   VSP <= '0';
                VP <= '0';   VS <= '0';
            when others =>
                RP <= '0';   RS <= '1';   RPP <= '1';   RSP <= '0';
                AP <= '0';   AS <= '0';   VPP <= '0';   VSP <= '1';
                VP <= '1';   VS <= '0';
        end case;
    end process;
end Behavioral;